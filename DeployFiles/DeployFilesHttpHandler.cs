﻿using System;
using System.Web;
using System.IO.Compression;

namespace DeployFiles
{
    public class DeployFilesHttpHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            var response = context.Response;
            response.ContentType = "application/zip";
            var archive = new ZipArchive(context.Request.InputStream);

            foreach (var entry in archive.Entries)
            {
                Console.WriteLine(entry.FullName);
            }

        }

        public bool IsReusable { get { return true; } }
    }
}
