﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DeployFiles
{
    public class ChecksumFile
    {
        private static readonly Regex ChecksumRegex = new Regex("(^[a-fA-F0-9]+)..(.+)$");
        private readonly IList<string> _checksums;
        private readonly IList<FileInfo> _files;
        private readonly Func<HashAlgorithm> _hashAlgorithmCreator;
        private FileInfo _currentFile;
        private static readonly StringToHashAlgorithm[] StringToHashAlgorithms;
        private static readonly StringComparer Comparer;
        private readonly ChecksumProgress _checksumProgress;

        static ChecksumFile()
        {
            StringToHashAlgorithms = new[]
            {
                new StringToHashAlgorithm("sha512", SHA512.Create),
                new StringToHashAlgorithm("sha384", SHA384.Create),
                new StringToHashAlgorithm("sha256", SHA256.Create),
                new StringToHashAlgorithm("sha1", SHA1.Create),
                new StringToHashAlgorithm("md5", MD5.Create)
            };
            Comparer = StringComparer.InvariantCultureIgnoreCase;
        }

        private ChecksumFile(IList<FileInfo> files, IList<string> checksums, long dataSize, Func<HashAlgorithm> hashAlgorithmCreatorCreator)
        {
            _checksums = checksums;
            _files = files;
            _hashAlgorithmCreator = hashAlgorithmCreatorCreator;
            _checksumProgress = new ChecksumProgress { Total = dataSize };
        }

        public async Task VerifyChecksum(IProgress<ISumProgress> progress = null)
        {
            var report = progress != null
                ? new Action<long>(x =>
                {
                    _checksumProgress.Checked += x;
                    _checksumProgress.File = _currentFile;
                    progress.Report(_checksumProgress);
                })
                : x => { };

            for (var index = 0; index < _files.Count; index++)
            {
                var file = _files[index];
                var checksum = _checksums[index];
                _currentFile = file;
                using (var stream = new ChecksumStream(file.OpenRead(), _hashAlgorithmCreator(), new Progress<long>(report)))
                {
                    await stream.CopyToAsync(Stream.Null, 100 * 1024);
                    stream.Flush();
                    var calculatedChecksum = stream.Hash.ToHex().ToLowerInvariant();
                    if (Comparer.Compare(checksum, calculatedChecksum) != 0)
                        throw new Exception(string.Format("Checksum failed for {0}. Invalid hash, expected: {1}, was {2}!", _currentFile.FullName, checksum, calculatedChecksum));
                }
            }
        }

        public static ChecksumFile Create(string checksumFile, string rootPath)
        {
            var checksumFileInfo = new FileInfo(checksumFile);
            if (!checksumFileInfo.Exists || checksumFileInfo.Directory == null)
                throw new ArgumentException("Could not locate " + checksumFile + "!", "checksumFile");

            var stringToHashAlgorithm = StringToHashAlgorithms.FirstOrDefault(x => Comparer.Compare(checksumFileInfo.Extension, "." + x.Name) == 0);
            if (stringToHashAlgorithm == null)
                throw new ArgumentException("Could not determine the hash algorithm, supported extensions are: " + string.Join(", ", StringToHashAlgorithms.Select(x => x.Name)), "checksumFile");

            long dataSize = 0;
            var lineNumber = 0;
            var lines = File.ReadAllLines(checksumFile);
            var fileInfos = new FileInfo[lines.Length];
            var checksums = new string[lines.Length];
            for (; lineNumber < lines.Length; lineNumber++)
            {
                var line = lines[lineNumber];
                if (string.IsNullOrWhiteSpace(line)) continue;
                var match = ChecksumRegex.Match(line);
                if (match.Success == false)
                    throw new Exception(string.Format("Failed to parse the checksum file {0}. Error at line: {1}", checksumFile, lineNumber));
                var fileInfo = new FileInfo(Path.Combine(rootPath, match.Groups[2].Value));
                checksums[lineNumber] = match.Groups[1].Value.ToLowerInvariant();
                fileInfos[lineNumber] = fileInfo;
                dataSize += fileInfo.Length;
            }

            return new ChecksumFile(fileInfos, checksums, dataSize, stringToHashAlgorithm.GetHashAlgorithmCreator);
        }

        private class StringToHashAlgorithm
        {
            public StringToHashAlgorithm(string name, Func<HashAlgorithm> hashAlgorithmCreator)
            {
                Name = name;
                GetHashAlgorithmCreator = hashAlgorithmCreator;
            }

            public string Name { get; private set; }
            public Func<HashAlgorithm> GetHashAlgorithmCreator { get; private set; }
        }
    }

    public interface ISumProgress
    {
        FileInfo File { get; }
        long Checked { get; }
        long Total { get; }
    }

    internal class ChecksumProgress : ISumProgress
    {
        public FileInfo File { get; set; }
        public long Checked { get; set; }
        public long Total { get; set; }
    }
}
