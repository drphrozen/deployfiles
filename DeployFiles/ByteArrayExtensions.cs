﻿using System.Collections.Generic;

namespace DeployFiles
{
    public static class ByteArrayExtensions
    {
        public static string ToHex(this IList<byte> bytes)
        {
            var c = new char[bytes.Count * 2];
            for (var i = 0; i < bytes.Count; i++)
            {
                var b = bytes[i] >> 4;
                c[i * 2] = (char)(55 + b + (((b - 10) >> 31) & -7));
                b = bytes[i] & 0xF;
                c[i * 2 + 1] = (char)(55 + b + (((b - 10) >> 31) & -7));
            }
            return new string(c);
        }
        public static string ToHexLowerCase(this IList<byte> bytes)
        {
            var c = new char[bytes.Count * 2];
            for (var i = 0; i < bytes.Count; i++)
            {
                var b = bytes[i] >> 4;
                c[i * 2] = (char)(87 + b + (((b - 10) >> 31) & -39));
                b = bytes[i] & 0xF;
                c[i * 2 + 1] = (char)(87 + b + (((b - 10) >> 31) & -39));
            }
            return new string(c);
        }
    }
}
