//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Security.Cryptography;
//using System.Text.RegularExpressions;
//using SharpCompress.Archive;
//using SharpCompress.Archive.Zip;
//using SharpCompress.Common;
//using SharpCompress.Reader;

//namespace DeployFiles
//{
//    public static class DeployFiles
//    {
//        private static readonly Tuple<string, Func<HashAlgorithm>>[] HashAlgorithms;

//        static DeployFiles()
//        {
//            HashAlgorithms = new[]
//            {
//                Tuple.Create("sha512", (Func<HashAlgorithm>)SHA512.Create),
//                Tuple.Create("sha384", (Func<HashAlgorithm>)SHA384.Create),
//                Tuple.Create("sha256", (Func<HashAlgorithm>)SHA256.Create),
//                Tuple.Create("sha1", (Func<HashAlgorithm>)SHA1.Create),
//                Tuple.Create("md5", (Func<HashAlgorithm>)MD5.Create),
//            };
//        }

//        private static Tuple<Func<HashAlgorithm>, string> GetHashAlgorithm(string filename)
//        {
//            foreach (var x in HashAlgorithms)
//            {
//                var fullFilename = Path.Combine(filename, x.Item1);
//                if (File.Exists(Path.Combine(filename, x.Item1)))
//                    return Tuple.Create(x.Item2, fullFilename);
//            }
//            return null;
//        }

//        public static void Extract(string targetFolder, string filename)
//        {
//            using (var stream = File.OpenRead(filename))
//            {
//                var hashAlgorithmTuple = GetHashAlgorithm(filename);
//                if(hashAlgorithmTuple != null)
//                {
//                    var checksumRegex = new Regex("(^[a-fA-F0-9]+)..(.+)$");
//                    var hashAlgorithm = hashAlgorithmTuple.Item1;
//                    var checksumFile = hashAlgorithmTuple.Item2;
//                    var checksumMatches = File
//                        .ReadAllLines(checksumFile)
//                        .Where(x => string.IsNullOrEmpty(x) == false)
//                        .Select(x => checksumRegex.Match(x)).ToArray();

//                    if (checksumMatches.All(x => x.Success) == false)
//                        throw new Exception(string.Format("Failed to parse checksum file {0}. Error at: {1}", checksumFile, checksumMatches.First(x => x.Success == false).Captures[0].Value));

//                    foreach (var checksumMatch in checksumMatches)
//                    {
//                        var hash = checksumMatch.Groups[1].Value;
//                        var path = checksumMatch.Groups[2].Value;
//                        using(File.OpenRead(Path.Combine(targetFolder, path)))
//                        var calculatedHash = hashAlgorithm().ComputeHash(stream).ToHex();
//                        if (StringComparer.InvariantCultureIgnoreCase.Compare(hash, calculatedHash) != 0)
//                            throw new ArgumentException(
//                                string.Format("Invalid hash, expected: {0}, was {1}!", hash, calculatedHash), "hash");
//                    }

                    
//                    stream.Seek(0, SeekOrigin.Begin);
//                }

//                var archive = ArchiveFactory.Open(stream);
//                var reader = archive.ExtractAllEntries();
//                reader.WriteAllToDirectory(targetFolder, ExtractOptions.ExtractFullPath | ExtractOptions.Overwrite);

//                var calculatedHash = checksumStream.Hash.ToHex();
//                if (StringComparer.InvariantCultureIgnoreCase.Compare(hash, calculatedHash) != 0)
//                    throw new ArgumentException(
//                        string.Format("Invalid hash, expected: {0}, was {1}!", hash, calculatedHash), "hash");
//                stream.CopyTo(Stream.Null);
//            }
//        }

//        private static void Pack(string folder, Stream stream)
//        {
//            using (var archive = ZipArchive.Create())
//            {
//                archive.AddAllFromDirectory(folder);
//                archive.SaveTo(stream, CompressionType.LZMA);
//            }
//        }

//        public static void Pack(string folder, Stream stream, out string hash)
//        {
//            var buffer = new MemoryStream();
//            Pack(folder, buffer);

//            buffer.Seek(0, SeekOrigin.Begin);
//            hash = CreateHash(buffer);

//            buffer.Seek(0, SeekOrigin.Begin);
//            buffer.CopyTo(stream);
//        }

//        private static string CreateHash(Stream stream)
//        {
//            return SHA256.Create().ComputeHash(stream).ToHex();
//        }

//    }
//}