﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace DeployFiles
{
    public class ChecksumStream : Stream
    {
        private readonly Stream _stream;
        private readonly HashAlgorithm _hashAlgorithm;
        private readonly IProgress<long> _progress;

        public byte[] Hash { get; private set; }

        public ChecksumStream(Stream stream, HashAlgorithm hashAlgorithm, IProgress<long> progress)
        {
            _stream = stream;
            _hashAlgorithm = hashAlgorithm;
            _progress = progress;
        }

        public override void Flush()
        {
            _stream.Flush();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _stream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _stream.SetLength(value);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            var value = _stream.Read(buffer, offset, count);

            if (value != 0)
            {
                _hashAlgorithm.TransformBlock(buffer, offset, value, null, 0);
                _progress.Report(Position);
            }
            else
            {
                _hashAlgorithm.TransformFinalBlock(buffer, offset, value);
                Hash = _hashAlgorithm.Hash;
            }

            return value;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _stream.Write(buffer, offset, count);
        }

        public override bool CanRead
        {
            get { return _stream.CanRead; }
        }

        public override bool CanSeek
        {
            get { return _stream.CanSeek; }
        }

        public override bool CanWrite
        {
            get { return _stream.CanWrite; }
        }

        public override long Length
        {
            get { return _stream.Length; }
        }

        public override long Position { get { return _stream.Position; } set { _stream.Position = value; } }
    }
}
