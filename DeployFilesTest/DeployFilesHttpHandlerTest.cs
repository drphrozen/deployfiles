﻿//using System;
//using System.IO;
//using System.Security.Cryptography;
//using DeployFiles;
//using NUnit.Framework;

//namespace DeployFilesTest
//{
//    [TestFixture]
//    public class DeployFilesHttpHandlerTest
//    {
//        public DeployFilesHttpHandlerTest(IProgress<ChecksumFile.SumProgress> progress)
//        {
//            _progress = progress;
//        }

//        private IProgress<ChecksumFile.SumProgress> _progress;

//        [Test]
//        public void TestCaseChecksumStream()
//        {
//            using (var stream = File.OpenRead(@"data\inetpub.7z"))
//            {
//                var checksumStream = new ChecksumStream(stream, SHA256.Create(), _progress);
//                checksumStream.CopyTo(Stream.Null);
//                // ReSharper disable once StringLiteralTypo
//                Assert.AreEqual(@"cdd9f711e10c026f3cec7572ed168efb2d5266b4f876a172b2128ee57fcbb8f3", checksumStream.Hash.ToHexLowerCase());
//            }
//        }

//        [Test]
//        public void TestCase7Zip()
//        {
//            // ReSharper disable once StringLiteralTypo
//            DeployFiles.DeployFiles.Deploy(@"C:\temp\deployfiles", File.OpenRead(@"data\inetpub.7z"), "cdd9f711e10c026f3cec7572ed168efb2d5266b4f876a172b2128ee57fcbb8f3");
//        }

//        [Test]
//        public void TestCaseZip()
//        {
//            // ReSharper disable once StringLiteralTypo
//            DeployFiles.DeployFiles.Deploy(@"C:\temp\deployfiles", File.OpenRead(@"data\inetpub.zip"), "dc66ab1f79c4d202e5f547db4267b65b6a3222af09f27d60e8c8d41fbef517c8");
//        }

//        [Test]
//        [ExpectedException(typeof(ArgumentException))]
//        public void TestCaseZipInvalidHash()
//        {
//            // ReSharper disable once StringLiteralTypo
//            DeployFiles.DeployFiles.Deploy(@"C:\temp\deployfiles", File.OpenRead(@"data\inetpub.zip"), "000082e58e30e0849ca943c1e9d99155390278494d35d6653a0cba684cc3759e");
//        }

//        [Test]
//        public void PackZip()
//        {
//            var buffer = new MemoryStream();

//            string hash;
//            DeployFiles.DeployFiles.Pack(Path.GetFullPath("."), buffer, out hash);

//            buffer.Seek(0, SeekOrigin.Begin);
//            DeployFiles.DeployFiles.Deploy(@"C:\temp\deployfiles", buffer, hash);
//        }
//    }
//}