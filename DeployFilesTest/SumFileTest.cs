﻿using System;
using DeployFiles;
using NUnit.Framework;

namespace DeployFilesTest
{
    [TestFixture]
    public class SumFileTest
    {
        [Test]
        public async void TestMd5SumFile()
        {
            var sumFile = ChecksumFile.Create(@"data\inetpub.md5", @"data");
            await sumFile.VerifyChecksum(new Progress<ISumProgress>(x => Console.WriteLine("{0,-10:P}{1}", (double)x.Checked / x.Total, x.File.Name)));
        }
        
        [Test]
        public async void TestSha512SumFile()
        {
            var sumFile = ChecksumFile.Create(@"data\inetpub.sha512", @"data");
            await sumFile.VerifyChecksum(new Progress<ISumProgress>(x => Console.WriteLine("{0,-10:P}{1}", (double)x.Checked/x.Total, x.File.Name)));
        }
    }
}